//Creating a new scene
let scene = new THREE.Scene();

//create a camera and setting it
let camera = new THREE.PerspectiveCamera(40, window.innerWidth / window.innerHeight, 1, 10000);

//arrangement camera setting according to object and environment view
camera.rotation.y = 45 / 180 * Math.PI;
camera.position.x = 800;
camera.position.y = 100;
camera.position.z = 1000;

//Adding WebGL renderer and setting 
let renderer = new THREE.WebGLRenderer({
    antialias: true
});
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.setPixelRatio(window.devicePixelRatio);
document.body.appendChild(renderer.domElement);


//Adding AmbientLight here
let ambientLight = new THREE.AmbientLight(0X404040, 2);
scene.add(ambientLight);
scene.background = new THREE.Color(0xdddddd);




// Adding directional light
let directionalLight = new THREE.DirectionalLight(0xffffff, 100);
directionalLight.position.set(0, 1, 0);
directionalLight.castShadow = true;
scene.add(directionalLight);



//loading orbit controller
let controller = new THREE.OrbitControls(camera, renderer.domElement);


controller.keys = {
	LEFT: THREE.MOUSE.ROTATE,
	MIDDLE: THREE.MOUSE.DOLLY,
	RIGHT: THREE.MOUSE.PAN
}

//Adding Loader here
var loader = new THREE.GLTFLoader();

loader.load('scene.gltf', function (gltf) {
    car = gltf.scene.children[0];
    car.scale.set(0.5, 0.5, 0.5);
    scene.add(gltf.scene);
    renderer.render(scene, camera)

}, undefined, function (error) {

    console.error(error);

});


//Creating Loop Animation 
const animate = () => {
    renderer.render(scene, camera);
    requestAnimationFrame(animate);
}

animate();

const onWindowResize = () => {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight)
}


window.addEventListener('resize', onWindowResize);